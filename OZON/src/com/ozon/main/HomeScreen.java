package com.ozon.main;

import android.app.ActionBar;
import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ozon.common.BluetoothDeviceActor;
import com.ozon.common.Utils;

public class HomeScreen extends Activity {

	private BluetoothDeviceActor bda;
	private TextView txtconnectStatus;
	private TextView txtBondStatus;
	private Menu mMenu;

	private BluetoothDevice mBluetoothdevice;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.homeactivity);

		initialization();

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		registerReceiver(mBluetoothGattReceiver, Utils.makeIntentFilter());

		bda = new BluetoothDeviceActor();
		bda.initialization(HomeScreen.this);

		connectStatus(getString(R.string.connecting));

		mBluetoothdevice = Utils.device;

		if (mBluetoothdevice != null) {
			if (mBluetoothdevice.getBondState() == BluetoothDevice.BOND_BONDED)
				txtBondStatus.setText(getString(R.string.bonded));
			else
				txtBondStatus.setText(getString(R.string.notbonded));

			bda.connectedDevice(mBluetoothdevice);

			if (actionBar != null) {
				actionBar.setTitle(mBluetoothdevice.getName());
			}

		}

	}

	private void connectStatus(String connectstatus) {
		// TODO Auto-generated method stub

		txtconnectStatus.setText(getString(R.string.status) + connectstatus);
	}

	private void initialization() {
		// TODO Auto-generated method stub
		txtBondStatus = (TextView) findViewById(R.id.serviceactivity_txt_bondstatus);
		txtconnectStatus = (TextView) findViewById(R.id.serviceactivity_txt_status);

		int upImageViewId = getResources().getIdentifier("android:id/up", null,
				null);// ImageView hosted in LinearLayout or HomeView
		ImageView backView = (ImageView) findViewById(upImageViewId);
		android.widget.FrameLayout.LayoutParams param = new android.widget.FrameLayout.LayoutParams(
				android.widget.FrameLayout.LayoutParams.WRAP_CONTENT,
				android.widget.FrameLayout.LayoutParams.WRAP_CONTENT);
		param.setMargins(0, 0, 7, 0);
		backView.setLayoutParams(param);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub

		mMenu = menu;

		getMenuInflater().inflate(R.menu.homemenu, menu);

		updateMenu();

		return true;
	}

	private void updateMenu() {
		// TODO Auto-generated method stub

		if (mMenu != null) {

			if (bda.isConnected()) {
				mMenu.findItem(R.id.disconnect).setVisible(true);
				mMenu.findItem(R.id.connect).setVisible(false);
			} else {
				mMenu.findItem(R.id.disconnect).setVisible(false);
				mMenu.findItem(R.id.connect).setVisible(true);
			}
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.connect:
			connectStatus(getString(R.string.connecting));
			bda.connectedDevice(mBluetoothdevice);
			break;
		case R.id.disconnect:
			bda.disConnectedDevice();
			connectStatus(getString(R.string.disconnected));
			break;
		case android.R.id.home:
			finish();
		}
		invalidateOptionsMenu();
		return true;
	}

	private BroadcastReceiver mBluetoothGattReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			String action = intent.getAction();

			String value = intent.getStringExtra("value");

			if (action.equalsIgnoreCase(Utils.ACTION_GATT_CONNECTED)) {

				connectStatus(getString(R.string.connected));

				updateMenu();

			} else if (action.equalsIgnoreCase(Utils.ACTION_DEVICE_NOT_FOUND)) {

			} else if (action.equalsIgnoreCase(Utils.ACTION_GATT_DISCONNECTED)) {

				connectStatus(getString(R.string.disconnected));
				updateMenu();

			} else if (action.equalsIgnoreCase(Utils.ACTION_CONNECT_FAIL)) {
				connectStatus(getString(R.string.disconnected));
				updateMenu();

			} else if (action
					.equalsIgnoreCase(Utils.ACTION_GATT_SERVICE_DISCOVERED)) {
				connectStatus(getString(R.string.discoverService));
			} else if (action.equalsIgnoreCase(Utils.ACTION_NOTIFY)) {

				Toast t = Toast.makeText(HomeScreen.this,
						"Notification Enable", Toast.LENGTH_SHORT);
				t.show();

			} else if (action.equalsIgnoreCase(Utils.ACTION_NOTIFY_EVENT)) {

				Toast t = Toast.makeText(HomeScreen.this, "Notification Event",
						Toast.LENGTH_SHORT);
				t.show();

				if (bda != null) {
					bda.deviceIsReadyForCommunication("ReadData", 0, "Read");

				}

			} else if (action.equalsIgnoreCase(Utils.ACTION_READ)) {

				if (value != null && !value.equalsIgnoreCase("")) {
					Toast t = Toast.makeText(HomeScreen.this, "Read Value = "
							+ value, Toast.LENGTH_SHORT);
					t.show();
				} else {
					Toast t = Toast.makeText(HomeScreen.this, "Read Value = ",
							Toast.LENGTH_LONG);
					t.show();
				}

			}
		}
	};

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

		if (mBluetoothGattReceiver != null)
			unregisterReceiver(mBluetoothGattReceiver);

		if (bda != null) {
			if (bda.mBluetoothGatt != null) {
				bda.mBluetoothGatt.disconnect();
				if (bda.mBluetoothGatt != null)
					bda.mBluetoothGatt.close();
			}
		}

	}

}
