package com.ozon.main;

import android.app.ActionBar;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.ozon.adapter.DeviceListAdapter;
import com.ozon.common.ScanDevices;
import com.ozon.common.ScanInterface;
import com.ozon.common.Utils;
import com.ozon.model.ExtendedBluetoothDevice;

public class ScanActivity extends Activity implements ScanInterface,
		OnClickListener {

	private ListView deviceList;
	private DeviceListAdapter mAdapter;
	public static final int NO_RSSI = -1000;
	private ScanDevices scandevice;
	private TextView txtStatus;
	private TextView txtReset;
	private TextView txtSearching;
	private BluetoothAdapter btAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.scanactivity);

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		Initialization();

		setListener();

		btAdapter = BluetoothAdapter.getDefaultAdapter();
		if (btAdapter.isEnabled()) {

			startScanning();
		} else {

			Utils.showBluetoothDialog(getResources().getString(R.string.alert),
					getResources().getString(R.string.turn_BT),
					ScanActivity.this);
		}

	}

	private void setListener() {
		// TODO Auto-generated method stub
		txtReset.setOnClickListener(this);
	}

	private void Initialization() {
		// TODO Auto-generated method stub

		deviceList = (ListView) findViewById(R.id.scanActivity_list);
		deviceList
				.setAdapter(mAdapter = new DeviceListAdapter(this, deviceList));
		txtStatus = (TextView) findViewById(R.id.scanActivity_txt_status);
		txtReset = (TextView) findViewById(R.id.scanActivity_txt_reset);
		txtSearching = (TextView) findViewById(R.id.scanActivity_txt_searching);

		int upImageViewId = getResources().getIdentifier("android:id/up", null,
				null);// ImageView hosted in LinearLayout or HomeView
		ImageView backView = (ImageView) findViewById(upImageViewId);
		android.widget.FrameLayout.LayoutParams param = new android.widget.FrameLayout.LayoutParams(
				android.widget.FrameLayout.LayoutParams.WRAP_CONTENT,
				android.widget.FrameLayout.LayoutParams.WRAP_CONTENT);
		param.setMargins(0, 0, 7, 0);
		backView.setLayoutParams(param);

	}

	private void startScanning() {
		// TODO Auto-generated method stub

		txtStatus.setText(getString(R.string.status) + getString(R.string.startScanning));
		scandevice = new ScanDevices(ScanActivity.this);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case Utils.REQUEST_ENABLE_BT:
			btAdapter = BluetoothAdapter.getDefaultAdapter();
			if (btAdapter != null && btAdapter.isEnabled()) {
				startScanning();
				invalidateOptionsMenu();
			} else {

				Utils.showBluetoothDialog(getResources().getString(R.string.alert),
						getResources().getString(R.string.turn_BT),
						ScanActivity.this);
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void connectedDevice(BluetoothDevice device) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addDevice(BluetoothDevice scandevices, String Name, int rssi,
			Boolean isBonded) {
		// TODO Auto-generated method stub

		txtSearching.setVisibility(View.GONE);

		mAdapter.addOrUpdateDevice(new ExtendedBluetoothDevice(scandevices,
				Name, rssi, isBonded), scandevice);

	}

	@Override
	public void updateScannedDevice(final BluetoothDevice device, final int rssi) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				mAdapter.updateRssiOfBondedDevice(device, rssi);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getMenuInflater().inflate(R.menu.scanning, menu);
		if (scandevice != null && scandevice.isScanning) {
			menu.findItem(R.id.scanning_start).setVisible(false);
			menu.findItem(R.id.scanning_stop).setVisible(true);
			menu.findItem(R.id.scanning_indicator).setActionView(
					R.layout.progress_indicator);
			txtStatus.setText("Status: Start Scanning");
		} else {
			menu.findItem(R.id.scanning_start).setVisible(true);
			menu.findItem(R.id.scanning_stop).setVisible(false);
			menu.findItem(R.id.scanning_indicator).setActionView(null);
			menu.findItem(R.id.scanning_indicator).setVisible(false);
			txtStatus.setText("Status: Stop Scanning");
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.scanning_start:
			txtStatus.setText(getString(R.string.status) + getString(R.string.startScanning));

			if (mAdapter != null) {

				int size = mAdapter.getdevicelistSize();
				if (size == 0)
					txtSearching.setVisibility(View.VISIBLE);
			}

			scandevice.scanLEDevice(true);
			break;
		case R.id.scanning_stop:
			txtStatus.setText(getString(R.string.status) + getString(R.string.stopScanning));
			scandevice.stopScanning();
			break;
		case android.R.id.home:
			finish();
		}
		invalidateOptionsMenu();
		return true;
	}


	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

		if (scandevice.isScanning)
			scandevice.stopScanning();

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		if (v == txtReset) {

			mAdapter.resetDevice();
			scandevice.resetDeviceList();
			
			if (scandevice != null && !scandevice.isScanning) {
				if (mAdapter != null) {

					int size = mAdapter.getdevicelistSize();
					if (size == 0)
						txtSearching.setVisibility(View.VISIBLE);
				}
				scandevice.scanLEDevice(true);
				invalidateOptionsMenu();
			}
		}

	}

	@Override
	public void stopscan() {
		// TODO Auto-generated method stub
		
	}

}
