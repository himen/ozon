package com.ozon.adapter;

import java.util.ArrayList;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.ozon.common.ScanDevices;
import com.ozon.common.Utils;
import com.ozon.main.HomeScreen;
import com.ozon.main.R;
import com.ozon.model.ExtendedBluetoothDevice;

/**
 * DeviceListAdapter class is list adapter for showing scanned Devices name,
 * address and RSSI image based on RSSI values.
 */
public class DeviceListAdapter extends BaseAdapter {

	private final ArrayList<ExtendedBluetoothDevice> mListValues = new ArrayList<ExtendedBluetoothDevice>();
	private final Context mContext;
	private final ExtendedBluetoothDevice.AddressComparator comparator = new ExtendedBluetoothDevice.AddressComparator();

	private ListView list;

	public DeviceListAdapter(Context context, ListView deviceList) {
		mContext = context;
		list = deviceList;
	}

	public void updateRssiOfBondedDevice(BluetoothDevice device, int Rssi) {
		comparator.address = device.getAddress();
		final int indexInBonded = mListValues.indexOf(comparator);
		if (indexInBonded >= 0) {
			ExtendedBluetoothDevice previousDevice = mListValues
					.get(indexInBonded);
			previousDevice.rssi = Rssi;
			View v = null;
			v = list.getChildAt(indexInBonded - list.getFirstVisiblePosition());
			if (v != null) {
				TextView rssivalue = (TextView) v
						.findViewById(R.id.scandevice_list_item_Rssivalue);
				rssivalue.setText("" + previousDevice.rssi + " dBm");
			}
			return;
		}
	}

	public void addOrUpdateDevice(ExtendedBluetoothDevice device,
			ScanDevices scandevice) {

		mListValues.add(device);
		notifyDataSetChanged();
	}

	public void clearDevices() {
		mListValues.clear();
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return mListValues.size();
	}

	@Override
	public Object getItem(int position) {

		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View oldView, ViewGroup parent) {
		final LayoutInflater inflater = LayoutInflater.from(mContext);

		View view = oldView;

		if (view == null) {
			view = inflater.inflate(R.layout.scandevice_list_item, parent,
					false);
			final ViewHolder holder = new ViewHolder();
			holder.name = (TextView) view
					.findViewById(R.id.scandevice_list_item_deviceName);
			holder.address = (TextView) view
					.findViewById(R.id.scandevice_list_item_deviceAddress);
			holder.rssivalue = (TextView) view
					.findViewById(R.id.scandevice_list_item_Rssivalue);
			holder.btnConnect = (Button) view
					.findViewById(R.id.scandevice_list_item_btn_connect);
			view.setTag(holder);
		}

		if (mListValues != null && mListValues.size() > 0) {

			final ExtendedBluetoothDevice edxtendedDevice = mListValues
					.get(position);
			final ViewHolder holder = (ViewHolder) view.getTag();
			final String name = edxtendedDevice.name;
			holder.name.setText(name != null ? name : mContext
					.getString(R.string.not_available));
			holder.address.setText(edxtendedDevice.device.getAddress());
			holder.rssivalue.setText("" + edxtendedDevice.rssi + " dBm");
			holder.btnConnect.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					
					Intent i = new Intent(mContext,
							HomeScreen.class);
					Utils.device = edxtendedDevice.device;
					mContext.startActivity(i);
					
				}
			});
		}
		return view;
	}

	private class ViewHolder {
		private TextView name;
		private TextView address;
		private TextView rssivalue;
		private Button btnConnect;
	}

	public void resetDevice() {
		// TODO Auto-generated method stub
		mListValues.clear();
		notifyDataSetChanged();

	}

	public int getdevicelistSize() {
		// TODO Auto-generated method stub

		if (mListValues != null && mListValues.size() > 0)
			return mListValues.size();

		return 0;
	}

}
