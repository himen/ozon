package com.ozon.common;

import java.io.UnsupportedEncodingException;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;

public class Utils {
	
	public final static int REQUEST_ENABLE_BT = 1001;
	private static final int SHORTENED_LOCAL_NAME = 0x08;
	private static final int COMPLETE_LOCAL_NAME = 0x09;
	
	public final static String ACTION_GATT_CONNECTED = "COM.OZON.COMMON.ACTION_GATT_CONNECTED";
	public final static String ACTION_GATT_DISCONNECTED = "COM.OZON.COMMON.ACTION_GATT_DISCONNECTED";
	public final static String ACTION_DEVICE_NOT_FOUND = "COM.OZON.COMMON.ACTION_DEVICE_NOT_FOUND";
	public final static String ACTION_CONNECT_FAIL = "COM.OZON.COMMON.ACTION_CONNECT_FAIL";
	public final static String ACTION_DESCRIPTOR_WRITE = "COM.OZON.COMMON.ACTION_DESCRIPTOR_WRITE";
	public final static String ACTION_GATT_ERROR = "COM.OZON.COMMON.ACTION_GATT_ERROR";
	public final static String ACTION_BATTERY_LEVEL = "COM.OZON.COMMON.ACTION_BATTERY_LEVEL";
	public static String ACTION_GATT_SERVICE_DISCOVERED = "COM.OZON.COMMON.ACTION_GATT_SERVICE_DISCOVERED";
	public final static String ACTION_NOTIFY = "COM.OZON.COMMON.ACTION_NOTIFY";
	public final static String ACTION_NOTIFY_EVENT = "COM.OZON.COMMON.ACTION_NOTIFY_EVENT";
	public final static String ACTION_READ = "COM.OZON.COMMON.ACTION_READ";
	

	public final static String service1_UUID = "c04b4442-1d27-45fa-bbbd-abee4f1a8fcd";
	
	public final static String characteristics1_UUID_notify = "8c958c83-3626-4733-a28e-31f3a0dae216";
	public final static String characteristics2_UUID_read = "3d9d606d-75f3-41ea-ade0-b0690254b5dd";
	
	public static BluetoothAdapter btAdapter;

	public static BluetoothDeviceActor bda;
	public static BluetoothDevice device = null;


	public static ScanDevices scanDevice;
	
	public static BluetoothDevice selected_device;

	public static IntentFilter makeIntentFilter() {

		IntentFilter filter = new IntentFilter();
		filter.addAction(ACTION_GATT_CONNECTED);
		filter.addAction(ACTION_GATT_DISCONNECTED);
		filter.addAction(ACTION_DEVICE_NOT_FOUND);
		filter.addAction(ACTION_CONNECT_FAIL);
		filter.addAction(ACTION_DESCRIPTOR_WRITE);
		filter.addAction(ACTION_GATT_ERROR);
		filter.addAction(ACTION_BATTERY_LEVEL);
		filter.addAction(ACTION_GATT_SERVICE_DISCOVERED);
		filter.addAction(ACTION_NOTIFY);
		filter.addAction(ACTION_NOTIFY_EVENT);
		filter.addAction(ACTION_READ);
		
		return filter;
	}

	public static BluetoothAdapter getBTAdapter() {
		btAdapter = BluetoothAdapter.getDefaultAdapter();
		return btAdapter;
	}

	public static BluetoothDeviceActor getBda() {
		return bda;
	}

	public static void setBda(BluetoothDeviceActor bda) {
		Utils.bda = bda;
	}
	
	public static ScanDevices getScanDevice() {
		return scanDevice;
	}

	public static void setScanDevice(ScanDevices scanDevice) {
		Utils.scanDevice = scanDevice;
	}
	
	
	/**
	 * Decodes the device name from Complete Local Name or Shortened Local Name
	 * field in Advertisement packet. Ususally if should be done by
	 * {@link BluetoothDevice#getName()} method but some phones skips that, f.e.
	 * Sony Xperia Z1 (C6903) with Android 4.3 where getName() always returns
	 * <code>null</code>. In order to show the device name correctly we have to
	 * parse it manually :(
	 */
	public static String decodeDeviceName(byte[] data) {
		String name = null;
		int fieldLength, fieldName;
		int packetLength = data.length;
		for (int index = 0; index < packetLength; index++) {
			fieldLength = data[index];
			if (fieldLength == 0)
				break;
			fieldName = data[++index];

			if (fieldName == COMPLETE_LOCAL_NAME
					|| fieldName == SHORTENED_LOCAL_NAME) {
				name = decodeLocalName(data, index + 1, fieldLength - 1);
				break;
			}
			index += fieldLength - 1;
		}
		return name;
	}

	/**
	 * Decodes the local name
	 */
	public static String decodeLocalName(final byte[] data, final int start,
			final int length) {
		try {
			return new String(data, start, length, "UTF-8");
		} catch (final UnsupportedEncodingException e) {
			return null;
		} catch (final IndexOutOfBoundsException e) {
			return null;
		}
	}

	public static void showBluetoothDialog(String title, String msg,
			final Context context) {
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
		dialogBuilder.setPositiveButton("Settings",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						Intent enableIntent = new Intent(
								BluetoothAdapter.ACTION_REQUEST_ENABLE);
						((Activity) context).startActivityForResult(
								enableIntent, Utils.REQUEST_ENABLE_BT);
					}
				});
		dialogBuilder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
		dialogBuilder.setTitle(title);
		dialogBuilder.setMessage(msg);
		dialogBuilder.show();
	}
}
