package com.ozon.common;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BluetoothDeviceActor implements Runnable {

	private final static String TAG = BluetoothDeviceActor.class
			.getSimpleName();
	private Context mContext;
	private BluetoothManager mBluetoothManager;
	private BluetoothAdapter mBluetoothAdapter;
	public int deviceId = 0;
	public BluetoothGatt mBluetoothGatt;
	public String commandname;
	public int commandValue;
	private BluetoothDevice mDevice;
	public String deviceMacAddress;
	private boolean isConnected = false;
	private Thread thread;
	private Timer failTimer;
	private ConnectionFailerTask failerTask;

	public ScanDevices scan;
	public ScanInterface scanbleinterface;

	private Queue<BluetoothGattDescriptor> descriptorWriteQueue = new LinkedList<BluetoothGattDescriptor>();

	public int getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}

	public BluetoothGatt getmBluetoothGatt() {
		return mBluetoothGatt;
	}

	public void setmBluetoothGatt(BluetoothGatt mBluetoothGatt) {
		this.mBluetoothGatt = mBluetoothGatt;
	}

	public BluetoothDevice getmDevice() {
		return mDevice;
	}

	public void setmDevice(BluetoothDevice mDevice) {
		this.mDevice = mDevice;
	}

	public String getDeviceMacAddress() {
		return deviceMacAddress;
	}

	public void setDeviceMacAddress(String deviceMacAddress) {
		this.deviceMacAddress = deviceMacAddress;
	}

	public boolean isConnected() {
		return isConnected;
	}

	public void setConnected(boolean isConnected) {
		this.isConnected = isConnected;
	}

	@SuppressLint("NewApi")
	public void initialization(Context context) {

		mContext = context;
		mBluetoothManager = (BluetoothManager) context
				.getSystemService(Context.BLUETOOTH_SERVICE);
		mBluetoothAdapter = mBluetoothManager.getAdapter();

	}

	public void setContext(Context context) {
		mContext = context;

	}

	public void scanDeviceObject() {

		scan = new ScanDevices(mContext);
	}

	public void connectedDevice(BluetoothDevice device) {

		mDevice = device;
		setDeviceMacAddress(device.getAddress());
		startThread();
	}

	public void disConnectedDevice() {

		if (mBluetoothGatt != null)
			mBluetoothGatt.disconnect();
	}

	@Override
	public void run() {
		connectDevice(mDevice);
	}

	public void startThread() {
		thread = new Thread(this);
		thread.start();
	}

	public void stopThread() {
		if (thread != null) {
			final Thread tempThread = thread;
			thread = null;
			tempThread.interrupt();
		}
	}

	/**
	 * Connects to the GATT server hosted on the Bluetooth LE device.
	 * 
	 * @param device
	 *            The device address of the destination device.
	 * 
	 * @return Return true if the connection is initiated successfully. The
	 *         connection result is reported asynchronously through the
	 *         {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
	 *         callback.
	 */
	public boolean connectDevice(final BluetoothDevice device) {

		if (device == null) {
			return false;
		}

		failTimer = new Timer();
		failerTask = new ConnectionFailerTask();
		failTimer.schedule(failerTask, 10000, 50000);

		try {

			if (mContext != null) {

				mBluetoothGatt = device.connectGatt(mContext, false,
						mGattCallback);

				setmBluetoothGatt(mBluetoothGatt);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}

	// Implements callback methods for GATT events that the app cares about. For
	// example,
	// connection change and services discovered.
	public final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {

		@Override
		public void onConnectionStateChange(BluetoothGatt gatt, int status,
				int newState) {
			super.onConnectionStateChange(gatt, status, newState);

			String intentAction;

			if (newState == BluetoothProfile.STATE_CONNECTED) {
				intentAction = Utils.ACTION_GATT_SERVICE_DISCOVERED;
				try {
					setConnected(true);
					if (failerTask != null && failTimer != null) {
						failerTask.cancel();
						failTimer.cancel();
					}

					broadcastUpdate(intentAction, "");

					stopThread();

					if (getmBluetoothGatt() != null) {
						boolean discover = getmBluetoothGatt()
								.discoverServices();
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
				setConnected(false);
				intentAction = Utils.ACTION_GATT_DISCONNECTED;
				if (getmBluetoothGatt() != null)
					refreshDeviceCache(mBluetoothGatt);
				getmBluetoothGatt().close();
				setmBluetoothGatt(null);

				if (descriptorWriteQueue != null
						&& descriptorWriteQueue.size() > 0)
					descriptorWriteQueue.clear();

				commandname = "";
				broadcastUpdate(intentAction, "");

			}

		}

		@Override
		public void onServicesDiscovered(BluetoothGatt gatt, int status) {
			super.onServicesDiscovered(gatt, status);

			String intentAction = Utils.ACTION_GATT_CONNECTED;
			broadcastUpdate(intentAction, "");
			discoverServices(gatt.getServices());

		}

		@Override
		public void onCharacteristicRead(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic, int status) {
			// TODO Auto-generated method stub
			super.onCharacteristicRead(gatt, characteristic, status);
			byte[] data = characteristic.getValue();
			if (data != null) {
				String value = convertCharacteristicsValue(characteristic);
				broadcastUpdate(Utils.ACTION_READ, value);
			}
		}

		@Override
		public void onCharacteristicWrite(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic, int status) {
			// TODO Auto-generated method stub
			super.onCharacteristicWrite(gatt, characteristic, status);

		}

		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic) {
			// TODO Auto-generated method stub
			super.onCharacteristicChanged(gatt, characteristic);

			if (characteristic.getUuid().toString()
					.equalsIgnoreCase(Utils.characteristics1_UUID_notify)) {

				byte[] data = characteristic.getValue();
				if (data != null) {
					String value = convertCharacteristicsValue(characteristic);
					broadcastUpdate(Utils.ACTION_NOTIFY_EVENT, value);

				}
			}

		}

		@Override
		public void onDescriptorRead(BluetoothGatt gatt,
				BluetoothGattDescriptor descriptor, int status) {
			// TODO Auto-generated method stub
			super.onDescriptorRead(gatt, descriptor, status);

		}

		@Override
		public void onDescriptorWrite(BluetoothGatt gatt,
				BluetoothGattDescriptor descriptor, int status) {
			super.onDescriptorWrite(gatt, descriptor, status);

			if (status == 133) {
				broadcastUpdate(Utils.ACTION_GATT_ERROR, "");
				return;
			}

			if (descriptorWriteQueue != null && descriptorWriteQueue.size() > 0) {
				descriptorWriteQueue.remove(); // pop the item that we just

				// finishing writing
				// if there is more to write, do it!
				if (descriptorWriteQueue.size() > 0)
					writeGattDescriptor(descriptorWriteQueue.element());
				else {
					broadcastUpdate(Utils.ACTION_NOTIFY, "");
				}
			}

		}

	};

	public String convertCharacteristicsValue(
			BluetoothGattCharacteristic characteristic) {

		// For all other profiles, writes the data formatted in HEX.
		final byte[] data = characteristic.getValue();
		StringBuilder stringBuilder = null;
		if (data != null && data.length > 0) {
			stringBuilder = new StringBuilder(data.length);
			for (byte byteChar : data)
				stringBuilder.append(String.format("%02X", byteChar));
		}

		String value = stringBuilder.toString().trim();
		return value;
	}

	private void discoverServices(List<BluetoothGattService> list) {

		for (int i = 0; i < list.size(); i++) {

			if (list.get(i).getUuid().toString()
					.equalsIgnoreCase(Utils.service1_UUID)) {

				UUID serviceuid = UUID.fromString(Utils.service1_UUID);
				BluetoothGattService service = getmBluetoothGatt().getService(
						serviceuid);
				UUID characteristicuid_notify = UUID
						.fromString(Utils.characteristics1_UUID_notify);
				BluetoothGattCharacteristic characteristic = null;
				if (service != null) {
					characteristic = service
							.getCharacteristic(characteristicuid_notify);
					if (characteristic != null) {
						boolean isNotify = getmBluetoothGatt()
								.setCharacteristicNotification(characteristic,
										true);
						ArrayList<BluetoothGattDescriptor> gattdescriptor = (ArrayList<BluetoothGattDescriptor>) characteristic
								.getDescriptors();

						for (int k = 0; k < gattdescriptor.size(); k++) {

							BluetoothGattDescriptor descriptor = gattdescriptor
									.get(k);
							descriptorWriteQueue.add(descriptor);

						}

					}

				}

			}

		}

		if (descriptorWriteQueue.size() > 0) {
			writeGattDescriptor(descriptorWriteQueue.element());
		} else {
			broadcastUpdate(Utils.ACTION_NOTIFY, "");

		}

	}

	// Write gatt descriptor
	public void writeGattDescriptor(BluetoothGattDescriptor d) {
		d.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
		getmBluetoothGatt().writeDescriptor(d);
	}

	class ConnectionFailerTask extends TimerTask {
		@Override
		public void run() {
			broadcastUpdate(Utils.ACTION_CONNECT_FAIL, "");
			failerTask.cancel();
			failTimer.cancel();
		}
	}

	private void broadcastUpdate(final String action, String value) {
		final Intent intent = new Intent(action);
		intent.putExtra("value", value);
		mContext.sendBroadcast(intent);
	}

	public void setCommandName(String name, int dayago) {
		commandname = name;
	}

	// device is ready to for communition
	public void deviceIsReadyForCommunication(String name, int value,
			String operationType) {

		commandname = name;

		if (name != null && !name.equalsIgnoreCase("")) {
			checkAndPerformCommand(name, commandValue, operationType);
		}
	}

	// check if device is null or its bluetooth gatt is null.
	public void checkAndPerformCommand(String command, int value,
			String operationType) {

		if (mDevice == null && deviceMacAddress != null) {
			mDevice = mBluetoothAdapter.getRemoteDevice(deviceMacAddress);
		} else {

			if (mDevice == null && deviceMacAddress == null) {
				scan = new ScanDevices(mContext);
				scan.scanLEDevice(true);
				return;
			}
		}
		if (getmBluetoothGatt() == null) {
			scan = new ScanDevices(mContext);
			scan.scanLEDevice(true);
		} else {
			processCurrentModel(value, operationType);
		}
	}

	private void processCurrentModel(int value, String operationType) {

		UUID serviceuid = UUID.fromString(Utils.service1_UUID);
		BluetoothGattService service = getmBluetoothGatt().getService(
				serviceuid);
		UUID characteristicuid_notify = UUID
				.fromString(Utils.characteristics2_UUID_read);
		BluetoothGattCharacteristic characteristic = null;
		if (service != null) {
			characteristic = service
					.getCharacteristic(characteristicuid_notify);
			if (characteristic != null) {

				if (operationType.equalsIgnoreCase("Read")) {
					boolean read = getmBluetoothGatt().readCharacteristic(
							characteristic);
				}
			}
		}

	}

	private boolean refreshDeviceCache(BluetoothGatt gatt) {
		try {
			BluetoothGatt localBluetoothGatt = gatt;
			Method localMethod = localBluetoothGatt.getClass().getMethod(
					"refresh", new Class[0]);
			if (localMethod != null) {
				boolean bool = ((Boolean) localMethod.invoke(
						localBluetoothGatt, new Object[0])).booleanValue();
				return bool;
			}
		} catch (Exception localException) {
			Log.e(TAG, "An exception occured while refreshing device");
		}
		return false;
	}

}
