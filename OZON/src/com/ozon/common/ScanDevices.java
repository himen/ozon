package com.ozon.common;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.os.Build;
import android.util.Log;

public class ScanDevices {

	private BluetoothAdapter mBluetoothAdapter;
	private ArrayList<String> scanDeviceNameList;
	private Context context;
	public ScanInterface scaninterface;
	public boolean isDeviceConnected = false;
	private static final boolean DEVICE_NOT_BONDED = false;

	public boolean isScanning = false;

	@SuppressLint("NewApi")
	public ScanDevices(Context mcontext) {
		final BluetoothManager bluetoothManager = (BluetoothManager) mcontext
				.getSystemService(Context.BLUETOOTH_SERVICE);
		mBluetoothAdapter = bluetoothManager.getAdapter();
		scanDeviceNameList = new ArrayList<String>();
		context = mcontext;
		scaninterface = (ScanInterface) mcontext;
		scanLEDevice(true);

	}
	

	// Scan the LE Devices.
	@SuppressLint("NewApi")
	public void scanLEDevice(boolean enable) {
		isScanning = true;
		mBluetoothAdapter.startLeScan(mLeScanCallback);
	}

	public void stopScanning() {
		try {
			mBluetoothAdapter.stopLeScan(mLeScanCallback);
			isScanning = false;
		} catch (Exception e) {
			Log.e("StopScanning", e.getMessage());
		}
	}

	// Device scan callback.
	private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

		@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
		@Override
		public void onLeScan(final BluetoothDevice device, int rssi,
				final byte[] scanRecord) {

			if (device != null) {

				scaninterface.updateScannedDevice(device, rssi);
				if (scanDeviceNameList != null
						&& scanDeviceNameList.contains(device.getAddress()
								.trim())) {

				} else {
					scanDeviceNameList.add(device.getAddress().trim());
					scaninterface.addDevice(device,
							Utils.decodeDeviceName(scanRecord), rssi,
							DEVICE_NOT_BONDED);
				}
			}
		}

	};

	public void resetDeviceList() {
		// TODO Auto-generated method stub
		scanDeviceNameList.clear();
	}
}
