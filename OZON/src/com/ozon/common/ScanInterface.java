package com.ozon.common;

import android.bluetooth.BluetoothDevice;

public interface ScanInterface {
	
	public void addDevice(BluetoothDevice scandevices, String Name, int rssi,
			Boolean isBonded);
	public void updateScannedDevice(BluetoothDevice device, int rssi);
	public void connectedDevice(BluetoothDevice device);
	public void stopscan();
}
